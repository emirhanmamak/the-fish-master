using System;
using UnityEngine;

public class FishSpawner : MonoBehaviour
{
    [SerializeField] private Fish _fishPrefabs;
    [SerializeField] private Fish.FishType[] _fishTypes;

    private void Awake()
    {
        for (int i = 0; i < _fishTypes.Length; i++)
        {
            int num = 0;
            while (num < _fishTypes[i].fishCount)
            {
                Fish fish = UnityEngine.Object.Instantiate<Fish>(_fishPrefabs);
            }
        }
    }
}
