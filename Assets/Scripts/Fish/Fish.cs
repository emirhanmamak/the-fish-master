using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class Fish : MonoBehaviour
{
    private Fish.FishType _type;
    private CircleCollider2D _collider2D;
    private SpriteRenderer _SpriteRenderer;
    private float screenLeft;
    private Tweener _tweener;

    public Fish.FishType Type
    {
        get { return _type; }
        set
        {
            _type = value;
            _collider2D.radius = _type.colliderRadius;
            _SpriteRenderer.sprite = _type.SpriteTemplate;
        }
    }


    [Serializable]
    public class FishType
    {
        public int price;
        public float fishCount;
        public float minLenght;
        public float maxLenght;
        public Sprite SpriteTemplate;
        public float colliderRadius;
    }

    private void Awake()
    {
        _collider2D = GetComponent<CircleCollider2D>();
        _SpriteRenderer = GetComponent<SpriteRenderer>();
        screenLeft = Camera.main.ScreenToWorldPoint(Vector3.zero).x;
    }

    public void ResetFish()
    {
        if (_tweener != null)
        {
            _tweener.Kill(false);
        }

        float num = UnityEngine.Random.Range(_type.minLenght, _type.maxLenght);
        _collider2D.enabled = true;
        Vector3 position = transform.position;
        position.x = screenLeft;
        position.y = num;
        transform.position = position;
        float num2 = 1;

        float y = UnityEngine.Random.Range(num - num2, num + num2);
        Vector2 v = new Vector2(-position.x, y);
        float num3 = 3;
        float delay = UnityEngine.Random.Range(0, 2 * num3);
        _tweener = transform.DOMove(v, num3, false).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear).SetDelay(delay)
            .OnStepComplete(delegate
            {
                Vector3 localScale = transform.localScale;
                localScale.x = -localScale.x;
                transform.localScale = localScale;
            });
    }
    
    public void Hooked()
    {
        _collider2D.enabled = false;
        _tweener.Kill(false);
    }
}