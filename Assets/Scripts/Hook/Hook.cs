using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Hook : MonoBehaviour
{
    public Transform HookedTransform;
    private Camera mainCamera;
    private Collider2D _collider2D;
    private Tweener cameraTween;
    private int length;
    private int strength;
    private int fishCount;
    private bool canMove;

    private void Awake()
    {
        mainCamera = Camera.main;
        _collider2D = GetComponent<Collider2D>();
        //List<Fish>
    }

    private void Update()
    {
        if (canMove && Input.GetMouseButton(0))
        {
            Vector3 vector = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            Vector3 position = transform.position;
            position.x = vector.x;
            transform.position = position;
        }
    }

    public void StartFishing()
    {
        length = -50; //IDLE MANAGER
        strength = 3; //IDLE MANAGER
        fishCount = 0;
        float time = (-length) * 0.1f;
        cameraTween = mainCamera.transform.DOMoveY(length, 1 + time * 0.25f, false).OnUpdate(delegate
        {
            if (mainCamera.transform.position.y <= -11)
            {
                transform.SetParent(mainCamera.transform);
            }
        }).OnComplete(delegate
        {
            _collider2D.enabled = true;
            cameraTween = mainCamera.transform.DOMoveY(0, time * 5, false).OnUpdate(delegate
            {
                if (mainCamera.transform.position.y >= -25f)
                {
                    StopFishing();
                }
            }); 
        });
        //Screen(GAME)
        _collider2D.enabled = true;
        canMove = true;
    }

    public void StopFishing()
    {
        canMove = false;
        cameraTween.Kill(false);
        cameraTween = mainCamera.transform.DOMoveY(0, 2, false).OnUpdate(delegate
        {
            if (mainCamera.transform.position.y >= -11)
            {
                transform.SetParent(null);
                transform.position = new Vector2(transform.position.x, -6);
            }
        }).OnComplete(delegate
        {
            transform.position = Vector2.down * 6;
            _collider2D.enabled = true;
            int num = 0;
            //Clearing out the hook from fishes
            //Idle Manager Totalgain = numm;
            //Screenmanager end screen
        });
    }
}
